/* global it, describe, context */
'use strict';
const chai = require('chai');
const spies = require('chai-spies');
chai.use(spies);
const expect = require('chai').expect;

const Node = require('../src/KripkeStructure').Node;

describe('Kripke struture\'s node', () => {
  context('On imposition', () => {
    it('Given an atomic formula and a node containing that formula should impose the formula', () => {
      const formula = 'E';
      const root = new Node({
        label: 'r',
        formulas: [formula]
      });
      expect(root.isAtomicFormulaImposed(formula)).to.be.true;
    });

    it('Given an atomic formula and a node NOT containing that formula should NOT impose the formula', () => {
      const formula = 'E';
      const root = new Node({
        label: 'r',
        formulas: []
      });
      expect(root.isAtomicFormulaImposed(formula)).to.be.false;
    });
  });

  context('On conjunction', () => {
    it('Given 2 atomic formulas and a node containing that formulas should impose the conjunction', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: formulas
      });
      expect(root.isConjunctionImposed(formulas)).to.be.true;
    });

    it('Given 2 atomic formulas and a node NOT containing that formulas should NOT impose the conjunction', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: []
      });
      expect(root.isConjunctionImposed(formulas)).to.be.false;
    });
  });

  context('On alternative', () => {
    it('Given 2 atomic formulas and a node containing one formula should impose the alternative', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: [formulas[0]]
      });
      expect(root.isAlternativeImposed(formulas)).to.be.true;
    });

    it('Given 2 atomic formulas and a node NOT containing any formula should NOT impose the alternative', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: []
      });
      expect(root.isAlternativeImposed(formulas)).to.be.false;
    });
  });

  context('On implication', () => {
    it('Given 2 atomic formulas (implication) and an empty root should impose the implication', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: []
      });
      expect(root.isImplicationImposed(formulas[0], formulas[1])).to.be.true;
    });

    it('Given 2 atomic formulas (implication) and a root containing these formulas should impose the implication', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: formulas
      });
      expect(root.isImplicationImposed(formulas[0], formulas[1])).to.be.true;
    });

    it('Given 2 atomic formulas (implication) and a root containing only 1st formula should NOT impose the implication', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: [formulas[0]]
      });
      expect(root.isImplicationImposed(formulas[0], formulas[1])).to.be.false;
    });

    it('Given 2 atomic formulas (implication) and a root containing only 2nd formula should NOT impose the implication', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: [formulas[1]]
      });
      expect(root.isImplicationImposed(formulas[0], formulas[1])).to.be.true;
    });

    it('Given 2 atomic formulas (implication), empty root and a child node containing these formulas should impose the implication', () => {
      const formulas = ['E', 'F'];
      const root = new Node({
        label: 'r',
        formulas: [],
        children: [{
          label: 'v',
          formulas: [formulas]
        }]
      });
      expect(root.isImplicationImposed(formulas[0], formulas[1])).to.be.true;
    });
  });

  context('On negation', () => {
    it('Given an atomic formula and an empty root should impose negation of the formula ', () => {
      const formula = 'E';
      const root = new Node({
        label: 'r',
        formulas: []
      });
      expect(root.isNegationImposed(formula)).to.be.true;
    });

    it('Given an atomic formula and an empty root should impose negation of the formula ', () => {
      const formula = 'E';
      const root = new Node({
        label: 'r',
        formulas: [formula]
      });
      expect(root.isNegationImposed(formula)).to.be.false;
    });
  });

  context('On complex formulas', () => {
    it('Given an atomic formula should check for its imposition', () => {
      const node = new Node({
        label: 'node',
        formulas: []
      });
      let formula = 'E';

      let spy = chai.spy.on(node, 'isAtomicFormulaImposed');
      node.isFormulaImposed(formula);
      expect(spy).to.have.been.called();
    });

    it('Given a conjunction should check for conjunction', () => {
      const node = new Node({
        label: 'node',
        formulas: []
      });
      let formula = {
        conjunction: ['E', 'F']
      };

      let spy = chai.spy.on(node, 'isConjunctionImposed');
      node.isFormulaImposed(formula);
      expect(spy).to.have.been.called();
    });

    it('Given an alternative should check for alternative', () => {
      const node = new Node({
        label: 'node',
        formulas: []
      });
      let formula = {
        alternative: ['E', 'F']
      };

      let spy = chai.spy.on(node, 'isAlternativeImposed');
      node.isFormulaImposed(formula);
      expect(spy).to.have.been.called();
    });

    it('Given an implication should check for implication', () => {
      const node = new Node({
        label: 'node',
        formulas: []
      });
      let formula = {
        implication: {
          a: 'E',
          b: 'F'
        }
      };

      let spy = chai.spy.on(node, 'isImplicationImposed');
      node.isFormulaImposed(formula);
      expect(spy).to.have.been.called();
    });

    it('Given a negation should check for negation', () => {
      const node = new Node({
        label: 'node',
        formulas: []
      });
      let formula = {
        negation: 'E'
      };

      let spy = chai.spy.on(node, 'isNegationImposed');
      node.isFormulaImposed(formula);
      expect(spy).to.have.been.called();
    });
  });
});
