/* global it, describe, context */
'use strict';
const expect = require('chai').expect;
const app = require('../src/app');
const KripkeStructure = require('../src/KripkeStructure').KripkeStructure;

describe('Kripke structure verifier', () => {
  context('Given classical logic formula and Kripke structure', () => {
    const formulas = {
      a: [{
        negation: {
          negation: 'E'
        }
      }],
      b: ['E']
    };
    const input = require('./kripke1.json');
    const kripkeStructure = new KripkeStructure(input);
    it('Should verify if it is the counter-example in the intuitionistic logic', () => {
      expect(app.verify(formulas, kripkeStructure)).to.be.true;
    });
  });

  context('Given another classical logic formula and Kripke structure', () => {
    const formulas = {
      a: [{
        implication: {
          a: 'E',
          b: 'F'
        }
      }],
      b: [{
        alternative: [{
          negation: 'E'
        }, 'F']
      }]
    };
    const input = require('./kripke2.json');
    const kripkeStructure = new KripkeStructure(input);
    it('Should verify if it is the counter-example in the intuitionistic logic', () => {
      expect(app.verify(formulas, kripkeStructure)).to.be.true;
    });
  });

  context('Given yet another classical logic formula and Kripke structure', () => {
    const formulas = {
      a: [{
        negation: {
          conjunction: ['E', 'F']
        }
      }],
      b: [{
        alternative: [{
          negation: 'E'
        }, {
          negation: 'F'
        }]
      }]
    };
    const input = require('./kripke3.json');
    const kripkeStructure = new KripkeStructure(input);
    it('Should verify if it is the counter-example in the intuitionistic logic', () => {
      expect(app.verify(formulas, kripkeStructure)).to.be.true;
    });
  });
});
