'use strict';

function verify(formulas, kripkeStructure) {
  let root = kripkeStructure.root;

  for (let i = 0; i < formulas.a.length; i++) {
    let formula = formulas.a[i];
    if (!root.isFormulaImposed(formula)) {
      return false;
    }
  }

  for (let i = 0; i < formulas.b.length; i++) {
    let formula = formulas.b[i];
    if (root.isFormulaImposed(formula)) {
      return false;
    }
  }

  return true;
}

module.exports = { verify };
