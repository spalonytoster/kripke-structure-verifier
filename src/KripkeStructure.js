class KripkeStructure {
  constructor(kripkeStructureObject) {
    this.root = new Node(kripkeStructureObject);
  }
}

class Node {
  constructor(node) {
    this.label = node.label;

    // these are atomic formulas!
    this.formulas = node.formulas;
    this.parent = node.parent;

    this.children = [];
    if (node.children) {
      node.children.forEach((child) => {
        this.addChild(child);
      });
    }
  }

  addChild(node) {
    node.parent = this;
    let child = new Node(node);
    this.children.push(child);
  }

  isAtomicFormulaImposed(formula) {
    return this.formulas.includes(formula);
  }

  isConjunctionImposed(formulas) {
    for (let i = 0; i < formulas.length; i++) {
      if (!this.isFormulaImposed(formulas[i])) {
        return false;
      }
    }
    return true;
  }

  isAlternativeImposed(formulas) {
    for (let i = 0; i < formulas.length; i++) {
      if (this.isFormulaImposed(formulas[i])) {
        return true;
      }
    }
    return false;
  }

  isImplicationImposed(formula1, formula2) {
    let implicationCheck = (f1, f2, node) => {
      if (node.isFormulaImposed(f1) && !node.isFormulaImposed(f2)) {
        return false;
      }
      return true;
    };

    if (!implicationCheck(formula1, formula2, this)) {
      return false;
    }

    for (let i = 0; i < this.children.length; i++) {
      if (!implicationCheck(formula1, formula2, this.children[i])) {
        return false;
      }
    }

    return true;
  }

  isNegationImposed(formula) {
    let negationCheck = (f, node) => {
      if (node.isFormulaImposed(f)) {
        return false;
      }
      return true;
    };

    if (!negationCheck(formula, this)) {
      return false;
    }

    for (let i = 0; i < this.children.length; i++) {
      if (!negationCheck(formula, this.children[i])) {
        return false;
      }
    }

    return true;
  }

  isFormulaImposed(formula) {
    if (typeof formula === 'string') {
      return this.isAtomicFormulaImposed(formula);
    }
    else if (typeof formula === 'object') {
      let operator = Object.keys(formula)[0];
      switch (operator) {
      case 'conjunction':
        return this.isConjunctionImposed(formula.conjunction);
      case 'alternative':
        return this.isAlternativeImposed(formula.alternative);
      case 'implication':
        return this.isImplicationImposed(formula.implication.a, formula.implication.b);
      case 'negation':
        return this.isNegationImposed(formula.negation);
      default:
        return null;
      }
    }
  }
}

module.exports = {
  KripkeStructure,
  Node
};
